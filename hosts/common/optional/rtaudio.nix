# heavily based on musenix
{pkgs, ...}: {
  boot = {
    kernel.sysctl = {"vm.swappiness" = 10;};

    kernelModules = ["snd-seq" "snd-rawmidi"];

    # from what I can tell this will increase power consumption but reduce input latency
    # for a laptop I don't think I want this on
    # kernelParams = ["threadirqs"];
  };

  security.pam.loginLimits = [
    {
      domain = "@audio";
      item = "memlock";
      type = "-";
      value = "unlimited";
    }
    {
      domain = "@audio";
      item = "rtprio";
      type = "-";
      value = "99";
    }
    {
      domain = "@audio";
      item = "nofile";
      type = "soft";
      value = "99999";
    }
    {
      domain = "@audio";
      item = "nofile";
      type = "hard";
      value = "99999";
    }
  ];

  services.udev.extraRules = ''
    KERNEL=="rtc0", GROUP="audio"
    KERNEL=="hpet", GROUP="audio"
    DEVPATH=="/devices/virtual/misc/cpu_dma_latency", OWNER="root", GROUP="audio", MODE="0660"
  '';

  environment.systemPackages = [pkgs.rtcqs];
}
