{pkgs, ...}: {
  # special udev rules for via
  environment.systemPackages = [pkgs.via];
  services.udev.packages = [pkgs.via];
  # special udev rules for keychron k6 pro
  services.udev.extraRules = ''
    # blacklist for usb autosuspend
    ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="3434", ATTR{idProduct}=="0260", GOTO="power_usb_rules_end"

    ACTION=="add", SUBSYSTEM=="usb", TEST=="power/control", ATTR{power/control}="auto"
    LABEL="power_usb_rules_end"
  '';
}
