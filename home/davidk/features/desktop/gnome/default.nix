{
  imports = [
    ./extensions.nix
    ./settings.nix
    ./apps.nix
  ];

  xdg.mimeApps.enable = true;
}
