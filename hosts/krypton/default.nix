{inputs, ...}: {
  imports = [
    inputs.nixos-hardware.nixosModules.lenovo-thinkpad-x1-7th-gen
    ./hardware-configuration.nix

    ../common/global
    ../common/users/davidk

    ../common/optional/docker.nix
    ../common/optional/gnome.nix
    ../common/optional/nextdns.nix
    ../common/optional/pipewire.nix
    ../common/optional/rtaudio.nix
    ../common/optional/systemd-boot.nix
    ../common/optional/zerotier.nix
  ];

  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];

  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  networking = {
    hostName = "krypton";
    networkmanager.enable = true;
  };

  programs.dconf.enable = true;

  environment.sessionVariables.NIXOS_OZONE_WL = "1";
  system.stateVersion = "23.11";
}
