{pkgs, ...}: {
  programs.steam = {
    enable = true;
    gamescopeSession.enable = true;
    remotePlay.openFirewall = true;
    localNetworkGameTransfers.openFirewall = true;
    extraCompatPackages = with pkgs; [proton-ge-bin];
    extest.enable = true;
  };

  hardware.steam-hardware.enable = true;
  environment.systemPackages = with pkgs; [mangohud steam-run];
  programs.gamemode.enable = true;
}
