{
  qt = {
    enable = true;
    platformTheme = {
      name = "adwaita";
    };
    style = {
      name = "kvantum";
    };
  };
}
