{
  pkgs,
  lib,
  config,
  ...
}: {
  sops.secrets.nextdns = {
    sopsFile = ../secrets.yaml;
  };

  services.nextdns = {
    enable = lib.mkDefault true;
    arguments = [
      "-report-client-info"
      "-config-file"
      "${config.sops.secrets.nextdns.path}"
    ];
  };

  networking.nameservers = lib.mkDefault [
    "127.0.0.1"
    "::1"
  ];

  networking.networkmanager = lib.mkIf config.networking.networkmanager.enable {
    dispatcherScripts = [
      {
        source = pkgs.writeText "restart_nextdns_on_network_change" ''
          #!${pkgs.bash}/bin/bash

          logger "Restarting nextdns on network change"
          ${pkgs.systemd}/bin/systemctl restart nextdns
          logger "Nextdns restarted on network change"
        '';
        type = "basic";
      }
    ];
  };

  specialisation = {
    # there's a bootloader conf bug where having an
    # underscore breaks a regex
    # https://discourse.nixos.org/t/unable-to-nixos-rebuild-switch-boot-filenotfounderror/50227/9
    # this should prevent wackiness happeningin the future
    disablenextdns.configuration = {
      networking.nameservers = [];
      services.nextdns.enable = false;
      networking.networkmanager.dispatcherScripts = [];
    };
  };
}
