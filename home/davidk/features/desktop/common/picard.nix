{pkgs, ...}: {
  home.packages = with pkgs; [
    picard
    rsgain
  ];
}
