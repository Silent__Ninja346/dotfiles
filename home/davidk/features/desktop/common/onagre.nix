{pkgs, ...}: {
  home.packages = with pkgs; [
    onagre
  ];
  dconf.settings = {
    "org/gnome/mutter".center-new-windows = true;
    "org/gnome/settings-daemon/plugins/media-keys".custom-keybindings = [
      "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
    ];
    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
      binding = "<Super>slash";
      command = "onagre";
      name = "Onagre";
    };
  };
  xdg.configFile."onagre/theme.scss".text = ''
    .onagre {
      background: #242424;
      color: #ffffff;
      --exit-unfocused: true;
      --font-family: "Iosevka,Iosevka Nerd Font";
      border-color: #1E1E1E;
      border-width: 4px;
      padding: 5px;

      .container {
        .rows {
          --height: fill-portion 6;
          .row {
            --width: 392;

            .icon {
              padding-top: 4px;
            }

            .category-icon {
              padding-left: 5px;
              --icon-size: 11;
            }

            .title {
              font-size: 18px;
            }
          }

          .row-selected {
            --width: 392;
            border-radius: 8%;
            background:  #3A3A3A;

            .icon {
              padding-top: 4px;
            }

            .category-icon {
              padding-left: 5px;
              --icon-size: 11;
            }

            .title {
              font-size: 20px;
            }
          }
        }

        .search {
          border-radius: 5%;
          background: #1E1E1E;
          --height: fill-portion 1;
          padding: 4px;
          .input {
            font-size: 20px;
          }
        }

        .scrollable {
          width: 2px;
          border-radius: 5%;
          background: #c0c0c0;
          .scroller {
            width: 4px;
            color: #a1a1a1;
          }
        }
      }
    }

  '';
}
