{pkgs, ...}: let
  koboldnative =
    pkgs.koboldcpp.overrideAttrs
    (attrs: {
      NIX_CFLAGS_COMPILE = (attrs.NIX_CFLAGS_COMPILE or "") + "-march=native";
    });
in {
  imports = [
    ./global

    ./features/desktop/gnome
    ./features/desktop/digikam
    ./features/desktop/vscode
    ./features/desktop/common
    ./features/desktop/kitty.nix
    ./features/desktop/solaar.nix
  ];

  services.easyeffects = {
    enable = true;
  };

  home.packages = with pkgs; [
    alsa-scarlett-gui
    authenticator
    beeper
    brave
    eyedropper
    flowtime
    foliate
    fragments
    fstl
    helvum
    itch
    kdenlive
    koboldnative
    krita
    mupdf-headless
    obsidian
    obs-studio
    onlyoffice-bin_latest
    protonvpn-gui
    qalculate-gtk
    strawberry
    rnote
    soundconverter
    tenacity
    v4l-utils
    zeal
  ];
}
