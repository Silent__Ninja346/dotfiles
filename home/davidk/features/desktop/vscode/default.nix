{
  config,
  pkgs,
  ...
}: let
  vscode_dir = "${config.home.homeDirectory}/Projects/dotfiles/home/davidk/features/desktop/vscode";
in {
  # mutable symlinks to keybinds and settings
  xdg.configFile."Code/User/settings.json".source = config.lib.file.mkOutOfStoreSymlink "${vscode_dir}/settings.json";
  xdg.configFile."Code/User/keybindings.json".source = config.lib.file.mkOutOfStoreSymlink "${vscode_dir}/keybindings.json";

  home.packages = with pkgs; [
    nixd
    clang-tools
    rust-analyzer
  ];

  programs.vscode = {
    enable = true;
    package = pkgs.vscode-fhs;
    mutableExtensionsDir = true;
    extensions = with pkgs.vscode-extensions;
      [
        vscodevim.vim

        # cpp and rust
        rust-lang.rust-analyzer
        vadimcn.vscode-lldb
        llvm-vs-code-extensions.vscode-clangd
        ms-vscode.cmake-tools
        twxs.cmake

        # python
        ms-python.vscode-pylance

        # nix
        kamadorueda.alejandra
        jnoortheen.nix-ide
        mkhl.direnv

        # config formats
        tamasfe.even-better-toml
        redhat.vscode-yaml

        # markup formats
        yzhang.markdown-all-in-one
        esbenp.prettier-vscode
        myriad-dreamin.tinymist

        # shell scripting
        foxundermoon.shell-format
        timonwong.shellcheck

        # git
        eamodio.gitlens
        codezombiech.gitignore

        # editor theme
        jdinhlife.gruvbox

        # readability enhancements
        usernamehw.errorlens
        oderwat.indent-rainbow
        gruntfuggly.todo-tree
        mechatroner.rainbow-csv

        # misc
        streetsidesoftware.code-spell-checker
        skellock.just
        ms-vscode.hexeditor

        # vscode remote & docker
        ms-vscode-remote.remote-ssh
        ms-vscode-remote.remote-containers
        ms-azuretools.vscode-docker
        ms-vsliveshare.vsliveshare
      ]
      ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
          name = "vscode-versionlens";
          publisher = "pflannery";
          version = "1.9.2";
          sha256 = "sha256-2wmdfvD6j/gQ3IEAn1vKOOzbqlmbWhVM5qXlLoBmg3c=";
        }
        {
          name = "mesonbuild";
          publisher = "mesonbuild";
          version = "1.24.0";
          sha256 = "sha256-n7c2CUiTIej2Y/QMGWpv6anuCDjqpo2W+hJylfvvMVE=";
        }
        {
          name = "ruff";
          publisher = "charliermarsh";
          version = "2024.20.0";
          sha256 = "sha256-CqLmL8o+arki7UGWtZ/B6GQclWumLqgUqcPIXhG+Ays=";
        }
        {
          name = "markdown-oxide";
          publisher = "FelixZeller";
          version = "1.0.4";
          sha256 = "sha256-D5htffxv15O9F7fMJvy3+IR9h4THpn8Wiq6guhJz+PM=";
        }
        {
          name = "pfeifer-hurl";
          publisher = "JacobPfeifer";
          version = "1.3.2";
          sha256 = "sha256-syD7ppgSDoUU8ZV7dRbN1EQac/RTrYrB74rwEVQPwWI=";
        }
      ];
  };
}
