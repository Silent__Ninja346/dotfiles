{
  pkgs,
  config,
  lib,
  ...
}: {
  programs.gpg = {
    enable = true;
  };
  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
    sshKeys = ["749310D5C0AD718CF7D7A1EEB00A31480DC3AE66"];
    pinentryPackage =
      if config.gtk.enable
      then pkgs.pinentry-gnome3
      else pkgs.pinentry-tty;
  };
  home.packages = lib.optional config.gtk.enable pkgs.gcr;

  # weird gnome keyring bug I guess? fixes SSH_AUTH_SOCK not being correctly set
  xdg.configFile."autostart/gnome-keyring-ssh.desktop".text = ''
    [Desktop Entry]
    Type=Application
    Hidden=true
  '';
}
