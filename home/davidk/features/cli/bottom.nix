{
  programs.bottom = {
    enable = true;
    settings = {
      flags = {
        battery = true;
        enable_gpu = true;
        mem_as_value = true;
      };
      processes = {
        columns = [
          "pid"
          "name"
          "cpu%"
          "mem"
          "read"
          "write"
          "tread"
          "twrite"
          "user"
          "state"
        ];
      };
      row = [
        {
          ratio = 30;
          child = [
            {
              type = "mem";
            }
            {
              type = "cpu";
            }
          ];
        }
        {
          ratio = 40;
          child = [
            {
              type = "proc";
              default = true;
            }
          ];
        }
        {
          ratio = 30;
          child = [
            {
              type = "net";
            }
            {
              type = "temp";
            }
            {
              type = "batt";
            }
          ];
        }
      ];
    };
  };
}
