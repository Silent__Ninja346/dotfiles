{
  pkgs,
  inputs,
  ...
}: let
  fprintd_toggle = pkgs.writeShellApplication {
    name = "fprintd_toggle";
    runtimeInputs = with pkgs; [systemd coreutils gnugrep fprintd];
    text = ''
      if grep -Fq closed /proc/acpi/button/lid/LID0/state; then
          systemctl stop fprintd.service
          ln -s /dev/null /run/systemd/transient/fprintd.service
          systemctl daemon-reload
      else
          rm -f /run/systemd/transient/fprintd.service
          systemctl daemon-reload
          systemctl start fprintd.service
      fi
    '';
  };
  wifi_cycle = pkgs.writeShellApplication {
    name = "wifi_cycle";
    runtimeInputs = [pkgs.kmod];
    text = ''
      modprobe -r mt7921e
      modprobe mt7921e
    '';
  };
in {
  imports = [
    inputs.nixos-hardware.nixosModules.framework-13-7040-amd

    ./hardware-configuration.nix

    ../common/global
    ../common/users/davidk

    ../common/optional/docker.nix
    ../common/optional/gnome.nix
    ../common/optional/libvirt.nix
    ../common/optional/nextdns.nix
    ../common/optional/pipewire.nix
    ../common/optional/rtaudio.nix
    ../common/optional/steam.nix
    ../common/optional/systemd-boot.nix
    ../common/optional/via.nix
    ../common/optional/yubikey.nix
    ../common/optional/zerotier.nix
  ];

  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];

  hardware.wirelessRegulatoryDatabase = true;
  boot.extraModprobeConfig = ''
    options cfg80211 ieee70211_regdom="US"
  '';

  environment.systemPackages = [fprintd_toggle wifi_cycle];
  services.acpid = {
    enable = true;
    lidEventCommands = ''
      ${fprintd_toggle}/bin/fprintd_toggle
    '';
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.kernelParams = [
    "amdgpu.sg_display=0"
    "amdgpu.graphics_sg=0"
  ];

  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  services.fwupd.enable = true;

  networking = {
    hostName = "cobalt";
    networkmanager.enable = true;
    wireless.iwd.enable = true;
    networkmanager.wifi.backend = "iwd";
  };

  programs.dconf.enable = true;

  services.printing.enable = true;

  services.syncthing = {
    enable = true;
    user = "davidk";
    overrideDevices = true;
    overrideFolders = true;
    dataDir = "/home/davidk/Syncthing";
    configDir = "/home/davidk/.config/syncthing";
    settings = {
      devices = {
        "Pixel 7a" = {id = "AXMJAZZ-7GFOOS4-OGXZMEG-PKE2S5W-SVT6LQ6-NDWC66C-BGIFRFB-ZTXEBAK";};
        "openmediavault" = {id = "5YWXOTU-BUUOPWD-JZEOL44-GIAG24Z-Q5AICYQ-LS2AJV6-Y4GFXJN-J7CJXQM";};
      };
      folders = {
        "Documents" = {
          id = "iciw3-dp6ao";
          path = "/home/davidk/Documents";
          devices = ["Pixel 7a" "openmediavault"];
          fsWatcherEnabled = true;
        };
        "Music" = {
          id = "1d08s-yzufj";
          path = "/home/davidk/Music";
          devices = ["Pixel 7a" "openmediavault"];
          fsWatcherEnabled = true;
        };
        "Pictures" = {
          id = "8229p-8z3tm";
          path = "/home/davidk/Pictures";
          devices = ["Pixel 7a" "openmediavault"];
          fsWatcherEnabled = true;
        };
        "Videos" = {
          id = "i4x8l-bnj2b";
          path = "/home/davidk/Videos";
          devices = ["Pixel 7a" "openmediavault"];
          fsWatcherEnabled = true;
        };
        "st" = {
          id = "vsqw2-hhwbq";
          path = "/home/davidk/Projects/st";
          devices = ["openmediavault"];
          fsWatcherEnabled = true;
        };
      };
    };
  };

  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  hardware.logitech.wireless.enable = true;
  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;

  system.stateVersion = "23.11";
}
