{pkgs, ...}: {
  # gtk apps that integrate well
  home.packages = with pkgs; [
    dconf-editor
    gnome-frog # ocr
    resources # better system monitor
    gnome-firmware # firmware updater
    gnome-tweaks
    boxbuddy
  ];
}
