{
  pkgs,
  config,
  ...
}: let
  ifTheyExist = groups: builtins.filter (group: builtins.hasAttr group config.users.groups) groups;
in {
  imports = [
    ../gnome-profile-picture.nix
  ];

  users.mutableUsers = false;
  users.users.davidk = {
    isNormalUser = true;
    shell = pkgs.fish;
    description = "David Krauthamer";
    extraGroups =
      [
        "wheel"
        "video"
        "audio"
      ]
      ++ ifTheyExist [
        "networkmanager"
        "docker"
        "libvirtd"
      ];
    icon = ./davidk.png;
    hashedPasswordFile = "${config.sops.secrets.davidk_password.path}";
  };

  sops.secrets.davidk_password = {
    sopsFile = ../../secrets.yaml;
    neededForUsers = true;
  };

  home-manager.users.davidk = import ../../../../home/davidk/${config.networking.hostName}.nix;
}
