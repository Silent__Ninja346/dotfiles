{
  imports = [
    ./discord.nix
    ./firefox.nix
    ./gtk.nix
    ./onagre.nix
    ./pavucontrol.nix
    ./picard.nix
    ./ripdrag.nix
    ./qt.nix
    ./virt-manager.nix
  ];

  dconf.settings."org/gnome/desktop/interface".color-scheme = "prefer-dark";
}
