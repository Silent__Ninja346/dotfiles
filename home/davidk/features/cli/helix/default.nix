{
  pkgs,
  config,
  ...
}: let
  packages = with pkgs; [
    helix-gpt

    bash-language-server

    clang-tools
    rust-analyzer
    cmake-language-server
    gdb
    rr

    tinymist
    texlab
    markdown-oxide
    dprint

    pyright
    ruff
    ruff-lsp

    docker-compose-language-service

    nixd

    vscode-langservers-extracted
    yaml-language-server
    taplo
  ];
  helix_dir = "${config.home.homeDirectory}/Projects/dotfiles/home/davidk/features/cli/helix";
in {
  home.packages = packages;
  programs.helix = {
    enable = true;
    extraPackages = packages;
  };

  home.sessionVariables.EDITOR = "hx";

  xdg.configFile."helix/config.toml".source = config.lib.file.mkOutOfStoreSymlink "${helix_dir}/config.toml";
  xdg.configFile."helix/languages.toml".source = config.lib.file.mkOutOfStoreSymlink "${helix_dir}/languages.toml";
}
