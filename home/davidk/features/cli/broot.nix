{
  programs.broot = {
    enable = true;
    settings = {
      icon_theme = "nerdfont";
      preview_transformers = [
        {
          input_extensions = ["pdf"];
          output_extension = "png";
          mode = "image";
          command = [
            "mutool"
            "draw"
            "-w"
            "1000"
            "-o"
            "{output-path}"
            "{input-path}"
            # only do the first page, there's no way I know of to scroll the preview anyway
            "1"
          ];
        }
      ];
      verbs = [
        {
          invocation = "ripdrag";
          shortcut = "rd";
          apply_to = "any";
          external = "ripdrag {file}";
          leave_broot = false;
        }
        {
          invocation = "edit";
          key = "enter";
          shortcut = "e";
          execution = "hx {file}:{line}";
          apply_to = "text_file";
          leave_broot = false;
        }
        {
          invocation = "backup {version}";
          key = "ctrl-b";
          auto_exec = false;
          execution = "cp -r {file} {parent}/{file-stem}-{version}{file-dot-extension}";
          leave_broot = false;
        }
        {
          invocation = "terminal";
          key = "ctrl-t";
          execution = "fish";
          set_working_dir = true;
          leave_broot = false;
        }
        {
          key = "ctrl-j";
          cmd = ":toggle_stage;:line_down";
        }
        {
          key = "ctrl-k";
          cmd = ":toggle_stage;:line_up";
        }

        {
          key = "ctrl-p";
          execution = ":line_up";
        }
        {
          key = "ctrl-n";
          execution = ":line_down";
        }
        {
          key = "ctrl-h";
          execution = ":panel_left_no_open";
        }
        {
          key = "ctrl-l";
          execution = ":panel_right";
        }
        {
          key = "ctrl-u";
          execution = ":page_up";
        }
        {
          key = "ctrl-d";
          execution = ":page_down";
        }
      ];
    };
  };
}
