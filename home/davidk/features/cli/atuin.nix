{
  programs.atuin = {
    enable = true;
    settings = {
      auto_sync = true;
      update_check = false;
      sync_frequency = "5m";
      show_preview = true;
      sync.records = true;
    };
  };
}
