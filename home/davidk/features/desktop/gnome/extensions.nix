{
  pkgs,
  config,
  lib,
  ...
}: let
  extensions = with pkgs.gnomeExtensions; [
    alphabetical-app-grid
    appindicator
    caffeine
    solaar-extension
    easyeffects-preset-selector
    # clipboard-indicator
  ];
in {
  home.packages = extensions;
  home.activation = let
    extensions_dir = "${config.home.homeDirectory}/.local/share/gnome-shell/extensions";
    git_exe = "${pkgs.git}/bin/git";
    clipboard_dir = "${extensions_dir}/clipboard-indicator@tudmotu.com";
  in {
    download_clipboard = lib.hm.dag.entryAfter ["writeBoundary"] ''
      run mkdir -p ${extensions_dir}
      if [ ! -d "${clipboard_dir}" ]; then
        run ${git_exe} clone https://github.com/Snorch/gnome-shell-extension-clipboard-indicator.git ${clipboard_dir}
        run ${git_exe} -C ${clipboard_dir} checkout d51ae82e67424fa61101370a2b25e8bdc5e38bef
      fi
    '';
  };
  # extensions config
  dconf.settings = {
    "org/gnome/shell".enabled-extensions =
      map (extension: extension.extensionUuid) extensions ++ ["clipboard-indicator@tudmotu.com"];
    "org/gnome/shell".disabled-extensions = [];
  };
}
