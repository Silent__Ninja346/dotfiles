{pkgs ? import <nixpkgs> {}}: {
  rtcqs = pkgs.callPackage ./rtcqs {};
  koboldcpp = pkgs.callPackage ./koboldcpp {};
}
