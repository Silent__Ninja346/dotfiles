{
  pkgs,
  lib,
  ...
}: {
  # desktop environment
  services = {
    xserver = {
      enable = true;
      displayManager.gdm = {
        enable = true;
      };
      desktopManager.gnome = {
        enable = true;
      };
    };
  };

  # makes nautilus happy about getting video and audio metadata information
  environment.sessionVariables.GST_PLUGIN_SYSTEM_PATH_1_0 = lib.makeSearchPathOutput "lib" "lib/gstreamer-1.0" (with pkgs.gst_all_1; [
    gst-plugins-good
    gst-plugins-bad
    gst-plugins-ugly
    gst-libav
  ]);

  # remove xterm because I have two terms already
  services.xserver.excludePackages = [pkgs.xterm];

  # gnome packages to not install by default
  environment.gnome.excludePackages =
    (with pkgs; [
      gnome-tour
      gnome-connections
      cheese
      geary
      epiphany
      gnome-calendar
      gnome-font-viewer
    ])
    ++ (with pkgs.gnome; [
      gnome-music
      gnome-characters
      gnome-clocks
      gnome-contacts
      gnome-maps
      gnome-weather
    ]);
}
