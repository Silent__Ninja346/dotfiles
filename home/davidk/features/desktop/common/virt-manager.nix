{
  lib,
  osConfig,
  ...
}: {
  dconf.settings."org/virt-manager/virt-manager/connections" = lib.mkIf (osConfig.programs.virt-manager.enable) {
    autoconnect = ["qemu:///system"];
    uris = ["qemu:///system"];
  };
}
