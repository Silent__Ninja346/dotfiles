{pkgs, ...}: {
  home.packages = with pkgs; [
    exiftool
    digikam
    tesseract
  ];
}
