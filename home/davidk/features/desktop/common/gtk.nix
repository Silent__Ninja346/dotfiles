{pkgs, ...}: {
  home.packages = [pkgs.adw-gtk3];
  gtk = {
    enable = true;
    theme.name = "adw-gtk3-dark";
    cursorTheme.name = "Adwaita";
  };
}
