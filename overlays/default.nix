{inputs, ...}: {
  # for every flake input, aliases pkgs.inputs.${flake} to
  # inputs.${flake}.packages.${system} or
  # inputs.${flake}.legacyPackages.${system}
  flake-inputs = final: _: {
    inputs =
      builtins.mapAttrs (
        _: flake: let
          legacyPackages = (flake.legacyPackages or {}).${final.system} or {};
          packages = (flake.packages or {}).${final.system} or {};
        in
          if legacyPackages != {}
          then legacyPackages
          else packages
      )
      inputs;
  };

  # custom packages
  additions = final: _prev: import ../pkgs {pkgs = final;};

  # modifies existing packages
  modifications = final: prev: {};

  # When applied, the stable nixpkgs set (declared in the flake inputs) will be accessible through 'pkgs.stable'
  stable = final: _prev: {
    stable = import inputs.nixpkgs-stable {
      system = final.system;
      config.allowUnfree = true;
    };
  };
}
