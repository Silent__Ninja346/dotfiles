{
  lib,
  config,
  ...
}: let
  packageNames = map (p: p.pname or p.name or null) config.home.packages;
  hasPackage = name: lib.any (x: x == name) packageNames;

  has_tauon = hasPackage "tauon";
  has_beeper = hasPackage "beeper";
in {
  dconf.settings = {
    "org/gnome/mutter".dynamic-workspaces = true;
    "org/gnome/mutter".experimental-features = ["scale-monitor-framebuffer"];
    "ca/desrt/dconf-editor".show-warning = false;
    "org/gnome/desktop/peripherals/touchpad" = {
      tap-to-click = true;
    };
    "org/gnome/desktop/wm/preferences" = {
      resize-with-right-button = true;
    };
    "org/gnome/desktop/input-sources".xkb-options = ["terminate:ctrl_alt_bksp" "caps:escape_shifted_capslock"];
    "org/gnome/shell".favorite-apps =
      [
        "firefox.desktop"
        "kitty.desktop"
        "org.gnome.Nautilus.desktop"
        "beeper.desktop"
        "tauonmb.desktop"
      ]
      ++ (
        if has_beeper
        then ["beeper.desktop"]
        else []
      )
      ++ (
        if has_tauon
        then ["tauonmb.desktop"]
        else []
      );
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
      font-name = "Cantarell 12";
      document-font-name = "Cantarell 12";
    };
    "org/gnome/settings-daemon/plugins/color" = {
      night-light-enabled = true;
      night-light-schedule-automatic = true;
      night-light-temperature = 3700;
    };
  };
}
