{
  programs.git = {
    enable = true;
    signing = {
      key = "C8779A41D0474434";
      signByDefault = true;
    };
    userEmail = "dkrautha@pm.me";
    userName = "David Krauthamer";
    extraConfig = {
      init.defaultBranch = "main";
      commit.verbose = true;
      pull.rebase = false;
    };
    ignores = [
      ".direnv"
      "result"
    ];
  };
  programs.git.delta = {
    enable = true;
    options = {};
  };
  programs.gitui.enable = true;
  programs.lazygit.enable = true;
}
