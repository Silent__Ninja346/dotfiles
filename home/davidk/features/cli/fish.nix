{
  pkgs,
  lib,
  config,
  osConfig,
  ...
}: let
  inherit (lib) mkIf;

  packageNames = map (p: p.pname or p.name or null) config.home.packages;
  hasPackage = name: lib.any (x: x == name) packageNames;

  hasBat = hasPackage "bat";
  hasYtdl = hasPackage "yt-dlp";
  hasFwt = hasPackage "framework-tool";
  hasEza = hasPackage "eza";
  hasTrash = hasPackage "trashy";
  hasXdgutils = hasPackage "xdg-utils";
  hasB3sum = hasPackage "b3sum";
  hasRipdrag = hasPackage "ripdrag";
  hasNom = hasPackage "nix-output-monitor";
  hasV4l = hasPackage "v4l-utils";

  hasLazygit = config.programs.lazygit.enable;
  hasKitty = config.programs.kitty.enable;
  hasFzf = config.programs.fzf.enable;
  hasVscode = config.programs.vscode.enable;
  hasBroot = config.programs.broot.enable;

  hasNh = osConfig.programs.nh.enable;

  rd_string =
    if hasFzf
    then "ripdrag (fzf --multi)"
    else "ripdrag";
in {
  programs.fish = {
    enable = true;
    shellAbbrs = rec {
      nd =
        if hasNom
        then "nom develop -c $SHELL"
        else "nix develop -c $SHELL";
      nsn =
        if hasNom
        then "nom shell nixpkgs#"
        else "nix shell nixpkgs#";
      nfu = "nix flake update --commit-lock-file";
      nos = mkIf hasNh "nh os switch -a";
      nob = mkIf hasNh "nh os boot -a";
      note = mkIf hasNh "nh os test -a";

      ls = mkIf hasEza "eza";
      ll = mkIf hasEza "eza -l";
      la = mkIf hasEza "eza -a";
      lla = mkIf hasEza "eza -la";

      cik = mkIf hasKitty "clone-in-kitty --type os-window";
      ck = cik;
      ssh = mkIf hasKitty "kitten ssh";
      icat = mkIf hasKitty "kitten icat";

      dlaudio = mkIf hasYtdl "yt-dlp --extract-audio --audio-format opus --audio-quality 0 --cookies-from-browser firefox";

      fwt = mkIf hasFwt "sudo framework_tool --driver portio";
      clm = mkIf hasFwt "sudo framework_tool --driver portio --charge-limit";

      tp = mkIf hasTrash "trash put";
      tpf = mkIf (hasTrash && hasFzf) "trash put (fzf -m)";

      lg = mkIf hasLazygit "lazygit";

      o = mkIf hasXdgutils "xdg-open";

      cat = mkIf hasBat "bat";

      rd = mkIf hasRipdrag rd_string;

      c = mkIf hasVscode "code .";

      b = mkIf hasBroot "br";
      bl = mkIf hasBroot "br -sdp";
      bw = mkIf hasBroot "br -w";

      sstat = "systemctl status";
      sstar = "systemctl start";
      sstop = "systemctl stop";
      sres = "systemctl restart";
      sustat = "systemctl --user status";
      sustar = "systemctl --user start";
      sustop = "systemctl --user stop";
      sures = "systemctl --user restart";

      cb = mkIf hasV4l "v4l2-ctl -c brightness=";
    };

    functions = {
      fish_greeting = "";
      blake_renamer = mkIf hasB3sum {
        # TODO: use substitution for core utils too
        body = ''
          set tempfile /tmp/blake_renamer.tmp
          ${pkgs.b3sum}/bin/b3sum $argv >$tempfile

          for line in (cat $tempfile)
              set line_split (string split "  " $line)
              set hash $line_split[1]
              set old_path $line_split[2]
              set old_no_extension (path change-extension "" $old_path)
              set extension (path extension $old_path)
              if [ "$hash" != "$old_no_extension" ]
                  echo "moving $old_path to $hash$extension"
                  mv $old_path $hash$extension
              end
          end
        '';
      };
    };

    interactiveShellInit = ''
      fish_config theme choose "ayu Dark"
    '';

    plugins = [
      {
        name = "autopair.fish";
        src = pkgs.fetchFromGitHub {
          owner = "jorgebucaran";
          repo = "autopair.fish";
          rev = "4d1752ff5b39819ab58d7337c69220342e9de0e2";
          sha256 = "sha256-qt3t1iKRRNuiLWiVoiAYOu+9E7jsyECyIqZJ/oRIT1A=";
        };
      }
    ];
  };
}
