{pkgs, ...}: {
  imports = [
    ./global

    ./features/desktop/gnome
    ./features/desktop/common/firefox.nix
    ./features/desktop/common/gtk.nix
    ./features/desktop/common/qt.nix
    ./features/desktop/kitty.nix
  ];

  home.packages = with pkgs; [
    brave
    helvum
    soundconverter
    fragments
    v4l-utils
  ];
}
