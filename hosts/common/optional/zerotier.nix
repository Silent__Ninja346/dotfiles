{
  services.zerotierone = {
    enable = true;
    joinNetworks = [
      "a0cbf4b62a0fc201" # home
      "4601bd822e68441d" # machine shop
    ];
    localConf = {
      settings = {};
    };
  };
}
