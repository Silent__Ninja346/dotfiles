{
  programs.fd = {
    enable = true;
    ignores = [];
    hidden = false;
    extraOptions = [];
  };
}
