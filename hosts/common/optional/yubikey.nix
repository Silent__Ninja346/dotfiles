{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    yubioath-flutter
    zenity
    yubikey-manager-qt
  ];

  services.udev.packages = [pkgs.yubikey-personalization];
  services.pcscd.enable = true;
}
