{pkgs, ...}: {
  home.packages = with pkgs; [
    (nerdfonts.override {fonts = ["IntelOneMono"];})
    noto-fonts # for titlebar icons in kitty 0.34
  ];

  fonts.fontconfig.enable = true;

  programs.kitty = {
    enable = true;
    font = {
      name = "IntoneMono Nerd Font";
      size = 12;
    };
    settings = {
      allow_remote_control = "yes";
      shell = "${pkgs.fish}/bin/fish";
      wayland_titlebar_color = "background";
    };
    theme = "Adwaita dark";
    keybindings = {
      "ctrl+shift+h" = "launch --stdin-source=@screen_scrollback --type=overlay hx";
      "ctrl+shift+g" = "launch --stdin-source=@last_cmd_output --type=overlay hx";
      "ctrl+f2" = "detach_window";
      "ctrl+f3" = "detach_window new_tab";
      "ctrl+f4" = "detach_window ask";
      "ctrl+f5" = "detach_tab";
      "ctrl+f6" = "detach_tab ask";
    };
  };
}
